## Interface: 11303
## Version: 1.0.2
## Title: Sharingan
## Author: luc2
## Notes: Records COMBAT_LOG_EVENT_UNFILTERED.
## SavedVariables: SharinganDB
## OptionalDeps: Ace3
## X-Embeds: Ace3

embeds.xml
enUS.lua
frFR.lua
Main.lua
