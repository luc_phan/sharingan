local L = LibStub("AceLocale-3.0"):NewLocale("Sharingan", "enUS", true)

L["Sharingan enabled!"] = true
L["Enable"] = true
L["Enable/disable Sharingan"] = true

L["Recording"] = true
L["Enable/disable events recording"] = true
L["Events list"] = true
L["List of events recorded"] = true
L["Clear events"] = true
L["Clear list of events recorded"] = true

L["Notification"] = true
L["Enable/disable notifications"] = true
L["Default channels"] = true
L["Default solo channel"] = true
L["None"] = true
L["Chat window"] = true
L["Say"] = true
L["Group"] = true
L["Raid"] = true
L["Warning"] = true
L["Battleground"] = true

L["Default group channel"] = true
L["Default raid channel"] = true
L["Default solo instance channel"] = true
L["Default group instance channel"] = true
L["Default raid instance channel"] = true
L["Default battleground channel"] = true

L["Triggers"] = true
L["New trigger name"] = true
L["Add"] = true
L["Add a new trigger"] = true
L["Trigger name"] = true
L["Create a new trigger"] = true
L["Delete trigger"] = true
L["Delete trigger?"] = true
L["Invalid trigger name"] = true
L["Duplicate trigger name"] = true

L["Conditions"] = true
L["Trigger conditions"] = true
L["New condition name"] = true
L["Add a new condition"] = true
L["Invalid condition name"] = true
L["Condition name"] = true
L["Delete condition"] = true
L["Delete condition?"] = true

L["Parameters"] = true
L["Argument number"] = true
L["Invalid argument number"] = true
L["Operator"] = true
L["Equals"] = true
L["Contains"] = true
L["It's me!"] = true
L["Operand"] = true
L["{%s} = %s"] = true
L["{%s} contains %s"] = true
L["{%s} is me!"] = true

L["Action"] = true
L["Message"] = true
L["Channel"] = true
L["Default"] = true
L["You can use placeholders to insert arguments in message. Example: {5} interrupted {9}! (the spell was: {16})"] = true

L["{2} contains INTERRUPT"] = true
L["Interruptions"] = true
L["{5} interrupted {9} ! ({16})"] = true
