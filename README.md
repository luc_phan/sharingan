Sharingan
=========

Description
-----------

Sharingan is a **WoW Classic** add-on to announce combat events. 

Example with spell interruption :

![Image showing spell interruption messages](images/sharingan-interrupted.png "(French version of WOW Classic)")

Output channel can be:

- Chat window
- Say
- Group
- Raid
- Raid warning
- Battleground

Usage
-----

### Example 1: Create a trigger to announce spell interruptions

- Type `/sharingan` or `/sha` to open the main window.

![Image showing recording tab](images/sharingan-recording.png "(French version of WOW Classic)")

- Enable Sharingan and recording.
- Fight a mob and interrupt a spell during the fight.
- Disable recording.
- In the events list, find the interrupt event, for example:

```
- 1580722124.429, SPELL_INTERRUPT, false, Player-4744-01217852, Bassocambo, 1297, 0, Creature-0-4448-0-129-1562-000037E7BE, Mage de la Voile sanglante, 68168, 0, 0, Coup de pied, 1, 0, Boule de feu, 4
```

Argument 1 is `1580722124.429`, argument 2 is `SPELL_INTERRUPT`, argument 3 is `false`, etc.

A spell is interrupted when argument 2 is `SPELL_INTERRUPT`.

- Create a spell interruption trigger:

![Image showing notification tab](images/sharingan-notification.png "(French version of WOW Classic)")

- Create a condition for the trigger
    + Name: `argument 2 = SPELL_INTERRUPT` (for example)
    + Argument number: `2`
    + Operator: `Equals`
    + Operand: `SPELL_INTERRUPT`
- Set an action for the trigger
    + Message: `A spell was interrupted!` (for example)
    + Channel: `Default`
- Enable notifications
- Fight a mob and check the message appears when you interrupt a spell.
- Improve the message by inserting arguments: `{5} interrupted {9}! (the spell was: {16})`
    + Argument 5 is the interrupting person
    + Argument 9 is the interrupted person
    + Argument 16 is the interrupted spell

### Example 2: Create a trigger to announce Entangling Roots

In this example, I'm going to create a trigger to announce my character is casting **Entangling Roots**.

- Type `/sharingan` or `/sha` to open the main window.
- Enable Sharingan and recording.
- Fight a mob and cast **Entangling Roots**.
- Disable recording.
- In the events list, find the cast event, for example:

```
- 1580727943.767, SPELL_CAST_START, false, Player-4744-0116CED1, Lucdeux, 1297, 0, , nil, -2147483648, -2147483648, 0, Sarments, 8
```

- Create a spell trigger.
- Create a condition for the trigger
    + Name: `argument 2 = SPELL_CAST_START` (for example)
    + Argument number: `2`
    + Operator: `Equals`
    + Operand: `SPELL_CAST_START`
- Create second condition for the trigger
    + Name: `argument 13 = Sarments` (for example)
    + Argument number: `13`
    + Operator: `Equals`
    + Operand: `Sarments` (it's the french spell name for **Entangling Roots**)
- Create a third condition for the trigger
    + Name: `argument 5 = me` (for example)
    + Argument number: `5`
    + Operator: `It's me!`
- Set an action for the trigger
    + Message: `Mokuton!` (for example)
    + Channel: `Default`
- Enable notifications
- Fight a mob and check the message appears when you cast **Entangling Roots**.

Todo
----

- ~~Documentation~~
