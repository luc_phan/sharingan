-- #############
-- # Constants #
-- #############

local ADDON_NAME = 'Sharingan'
local VERSION = 'v1.0.2'

-- ###########
-- # Imports #
-- ###########

Sharingan = LibStub("AceAddon-3.0"):NewAddon(ADDON_NAME, "AceConsole-3.0", "AceEvent-3.0")

local L = LibStub("AceLocale-3.0"):GetLocale(ADDON_NAME)
local AceGUI = LibStub("AceGUI-3.0")
local AceConfig = LibStub("AceConfig-3.0")
local AceConfigDialog = LibStub("AceConfigDialog-3.0")

-- ############
-- # Defaults #
-- ############

-- declare defaults to be used in the DB
local defaults = {
  global = {
    enabled = false,
    recordingEnabled = false,
    events = '',
    notificationEnabled = false,
    defaultSoloChannel = 'chatWindow',
    defaultGroupChannel = 'chatWindow',
    defaultRaidChannel = 'chatWindow',
    defaultSoloInstanceChannel = 'chatWindow',
    defaultGroupInstanceChannel = 'chatWindow',
    defaultRaidInstanceChannel = 'chatWindow',
    defaultBattlegroundChannel = 'chatWindow',
    triggers = {
      -- Impossible to set default triggers: No spellId in event infos. Spell must be localized string.
    }
  }
}

-- ##############
-- # Main Frame #
-- ##############

function Sharingan:CreateMainFrame(mainFrameName)
  local MainFrame = AceGUI:Create("Frame")
  local title = ADDON_NAME .. ' ' .. VERSION
  MainFrame:SetTitle(title)
  MainFrame:SetStatusText(title)
  MainFrame:SetCallback("OnClose", function(widget) AceGUI:Release(widget) end) -- releases all widgets -_-
  MainFrame:SetLayout("Flow")
  -- MainFrame:Hide()
  MainFrame.frame:SetMovable(true)
  MainFrame.frame:SetScript(
    "OnMouseDown",
    function(button)
      MainFrame.frame:StartMoving()
      MainFrame.isMoving = true
    end
  )
  MainFrame.frame:SetScript(
    "OnMouseUp",
    function(button)
      if (MainFrame.isMoving) then
        -- MainFrame.frame:StopMovingOrSizing()
        MainFrame.isMoving = false

        -- local frame = mover:GetParent()
        local frame = MainFrame.frame
        frame:StopMovingOrSizing()
        local self = frame.obj
        local status = self.status or self.localstatus
        status.width = frame:GetWidth()
        status.height = frame:GetHeight()
        status.top = frame:GetTop()
        status.left = frame:GetLeft()
      end
    end
  )
  tinsert(UISpecialFrames, mainFrameName) -- allow ESC to close the window
  return MainFrame
end

function Sharingan:BuildMainFrame(mainFrameName)
  local MainFrame = self:CreateMainFrame(mainFrameName)
  AceConfigDialog:Open(self.name, MainFrame)
  -- local dropdownGroup = BuildDropdowngroup()
  -- MainFrame:AddChild(dropdownGroup)
  return MainFrame
end

-- ##########
-- # Add-on #
-- ##########

function Sharingan:InitOptions()
  local options = {
    name = self.name,
    handler = self,
    type = 'group',
    childGroups = 'tab',
    args = {
      enable = {
        name = L["Enable"],
        desc = L["Enable/disable Sharingan"],
        type = 'toggle',
        set = 'SetEnable',
        get = 'GetEnable'
      },
      recording = {
        order = 1,
        name = L["Recording"],
        type = 'group',
        args = {
          enableRecording = {
            name = L["Enable"],
            desc = L["Enable/disable events recording"],
            type = 'toggle',
            get = function()
              return self.db.global.recordingEnabled
            end,
            set = function(info, value)
              self.db.global.recordingEnabled = value
            end,
            order = 1
          },
          events = {
            name = L["Events list"],
            desc = L["List of events recorded"],
            type = 'input',
            multiline = 10,
            width = 'full',
            get = function()
              return self.db.global.events
            end,
            order = 2
          },
          clear = {
            name = L["Clear events"],
            desc = L["Clear list of events recorded"],
            type = 'execute',
            func = function()
              self.db.global.events = ''
            end,
            order = 3
          }
        }
      },
      announce = {
        order = 2,
        name = L["Notification"],
        type = 'group',
        args = {
          enableRecording = {
            name = L["Enable"],
            desc = L["Enable/disable notifications"],
            type = 'toggle',
            get = function()
              return self.db.global.notificationEnabled
            end,
            set = function(info, value)
              self.db.global.notificationEnabled = value
            end,
            order = 1
          },
          defaultChannels = {
            name = L["Default channels"],
            desc = L["Default channels"],
            type = 'group',
            inline = true,
            args = {
              defaultSoloChannel = {
                name = L["Default solo channel"],
                desc = L["Default solo channel"],
                type = 'select',
                values = {
                  none = L["None"],
                  chatWindow = L["Chat window"],
                  -- sayChannel = L["Say"], -- blocked by Blizzard outside instance
                },
                sorting = {
                  'none',
                  'chatWindow',
                  -- 'sayChannel',
                },
                get = function()
                  return self.db.global.defaultSoloChannel
                end,
                set = function(info, value)
                  self.db.global.defaultSoloChannel = value
                end,
                order = 1
              },
              defaultGroupChannel = {
                name = L["Default group channel"],
                desc = L["Default group channel"],
                type = 'select',
                values = {
                  none = L["None"],
                  chatWindow = L["Chat window"],
                  -- sayChannel = L["Say"],
                  groupChannel = L["Group"],
                },
                sorting = {
                  'none',
                  'chatWindow',
                  -- 'sayChannel',
                  'groupChannel',
                },
                get = function()
                  return self.db.global.defaultGroupChannel
                end,
                set = function(info, value)
                  self.db.global.defaultGroupChannel = value
                end,
                order = 2
              },
              defaultRaidChannel = {
                name = L["Default raid channel"],
                desc = L["Default raid channel"],
                type = 'select',
                values = {
                  none = L["None"],
                  chatWindow = L["Chat window"],
                  -- sayChannel = L["Say"],
                  groupChannel = L["Group"],
                  raidChannel = L["Raid"],
                  warningChannel = L["Warning"]
                },
                sorting = {
                  'none',
                  'chatWindow',
                  -- 'sayChannel',
                  'groupChannel',
                  'raidChannel',
                  'warningChannel'
                },
                get = function()
                  return self.db.global.defaultRaidChannel
                end,
                set = function(info, value)
                  self.db.global.defaultRaidChannel = value
                end,
                order = 3
              },
              defaultSoloInstanceChannel = {
                name = L["Default solo instance channel"],
                desc = L["Default solo instance channel"],
                type = 'select',
                values = {
                  none = L["None"],
                  chatWindow = L["Chat window"],
                  sayChannel = L["Say"],
                },
                sorting = {
                  'none',
                  'chatWindow',
                  'sayChannel',
                },
                get = function()
                  return self.db.global.defaultSoloInstanceChannel
                end,
                set = function(info, value)
                  self.db.global.defaultSoloInstanceChannel = value
                end,
                order = 4
              },
              defaultGroupInstanceChannel = {
                name = L["Default group instance channel"],
                desc = L["Default group instance channel"],
                type = 'select',
                values = {
                  none = L["None"],
                  chatWindow = L["Chat window"],
                  sayChannel = L["Say"],
                  groupChannel = L["Group"],
                },
                sorting = {
                  'none',
                  'chatWindow',
                  'sayChannel',
                  'groupChannel',
                },
                get = function()
                  return self.db.global.defaultGroupInstanceChannel
                end,
                set = function(info, value)
                  self.db.global.defaultGroupInstanceChannel = value
                end,
                order = 5
              },
              defaultRaidInstanceChannel = {
                name = L["Default raid instance channel"],
                desc = L["Default raid instance channel"],
                type = 'select',
                values = {
                  none = L["None"],
                  chatWindow = L["Chat window"],
                  sayChannel = L["Say"],
                  groupChannel = L["Group"],
                  raidChannel = L["Raid"],
                  warningChannel = L["Warning"]
                },
                sorting = {
                  'none',
                  'chatWindow',
                  'sayChannel',
                  'groupChannel',
                  'raidChannel',
                  'warningChannel'
                },
                get = function()
                  return self.db.global.defaultRaidInstanceChannel
                end,
                set = function(info, value)
                  self.db.global.defaultRaidInstanceChannel = value
                end,
                order = 6
              },
              defaultBattlegroundChannel = {
                name = L["Default battleground channel"],
                desc = L["Default battleground channel"],
                type = 'select',
                values = {
                  none = L["None"],
                  chatWindow = L["Chat window"],
                  sayChannel = L["Say"],
                  groupChannel = L["Group"],
                  raidChannel = L["Raid"],
                  warningChannel = L["Warning"],
                  battlegroundChannel = L["Battleground"]
                },
                sorting = {
                  'none',
                  'chatWindow',
                  'sayChannel',
                  'groupChannel',
                  'raidChannel',
                  'warningChannel',
                  'battlegroundChannel'
                },
                get = function()
                  return self.db.global.defaultBattlegroundChannel
                end,
                set = function(info, value)
                  self.db.global.defaultBattlegroundChannel = value
                end,
                order = 7
              },
            },
          },
          triggersHeader = {
            name = L["Triggers"],
            type = 'header'
          },
          triggerList = {
            name = L["Triggers"],
            desc = L["Triggers"],
            type = 'group',
            args = {
              triggerName = {
                name = L["New trigger name"],
                type = 'input',
                get = function()
                  return self.newTriggerName
                end,
                set = function(info, value)
                  -- for k, v in pairs(info) do
                  --   print(k, v)
                  -- end
                  self.newTriggerName = value
                end,
                -- pattern = '.+'
                order = 1
              },
              newTrigger = {
                name = L["Add"],
                desc = L["Add a new trigger"],
                type = 'execute',
                func = function()
                  local name = self.newTriggerName or ""
                  if not string.match(name, '%a') then
                    SharinganMainFrame:SetStatusText(L["Invalid trigger name"])
                    return
                  end

                  if self:TriggerNameExists(name) then
                    SharinganMainFrame:SetStatusText(L["Duplicate trigger name"])
                    return
                  end

                  local newTrigger = {
                    name = name,
                    conditions = {},
                    action = {}
                  }
                  table.insert(self.db.global.triggers, newTrigger)

                  self.newTriggerName = nil
                  self:InitOptions()
                end,
                order = 2
              },
            }
          },
        },
      }
    },
  }

  local triggerListContent = options.args.announce.args.triggerList.args

  local i, trigger
  for i, trigger in pairs(self.db.global.triggers) do
    -- print(i, trigger)
    local name = 'trigger' .. i
    triggerListContent[name] = {
      name = trigger.name or "",
      desc = trigger.action.message,
      type = 'group',
      order = i,
      args = {
        -- triggerName = {
        --   name = L["Trigger name"],
        --   type = 'input',
        --   -- width = 'full',
        --   get = function()
        --     return trigger.name
        --   end,
        --   order = 1
        -- },
        triggerDelete = {
          name = L["Delete trigger"],
          desc = L["Delete trigger"],
          type = 'execute',
          func = function()
            self:DeleteTrigger(trigger)
            self:InitOptions()
          end,
          confirm = true,
          confirmText = L["Delete trigger?"],
          order = 2
        },
        conditionsHeader = {
          name = L["Conditions"],
          desc = L["Trigger conditions"],
          type = 'header',
          order = 3
        },
        newConditionName = {
          type = 'input',
          name = L["New condition name"],
          get = function()
            return self.newConditionName
          end,
          set = function(info, value)
            self.newConditionName = value
          end,
          order = 4
        },
        newConditionAction = {
          type = 'execute',
          name = L["Add"],
          desc = L["Add a new condition"],
          func = function()
            local name = self.newConditionName or ""
            if not string.match(name, '%a') then
              SharinganMainFrame:SetStatusText(L["Invalid condition name"])
              return
            end

            if self:ConditionNameExists(name, trigger) then
              SharinganMainFrame:SetStatusText(L["Duplicate condition name"])
              return
            end

            local newCondition = {
              name = name,
              argumentNumber = nil,
              operator = nil,
              operand = nil
            }
            table.insert(trigger.conditions, newCondition)
            self.newConditionName = nil

            self:InitOptions()
          end,
          order = 5
        },
        actionHeader = {
          type = 'header',
          name = L["Action"],
          order = 7
        },
        message = {
          type = 'input',
          name = L["Message"],
          desc = trigger.action.message,
          get = function()
            return trigger.action.message
          end,
          set = function(info, value)
            trigger.action.message = value
            self:InitOptions()
          end,
          order = 8
        },
        channel = {
          type = 'select',
          name = L["Channel"],
          values = {
            default = L["Default"],
            none = L["None"],
            chatWindow = L["Chat window"],
            sayChannel = L["Say"],
            groupChannel = L["Group"],
            raidChannel = L["Raid"],
            warningChannel = L["Warning"]
          },
          sorting = {
            'default',
            'none',
            'chatWindow',
            'sayChannel',
            'groupChannel',
            'raidChannel',
            'warningChannel'
          },
          get = function()
            return trigger.action.channel
          end,
          set = function(info, value)
            trigger.action.channel = value
          end,
          order = 9
        },
        messageHelper = {
          type = 'description',
          name = L["You can use placeholders to insert arguments in message. Example: {5} interrupted {9}! (the spell was: {16})"],
          -- fontSize = 'large'
        }
      }
    }

    local triggerContent = triggerListContent[name].args

    local j, condition
    for j, condition in pairs(trigger.conditions) do
      local name = 'condition' .. j
      triggerContent[name] = {
        type = 'group',
        name = condition.name,
        desc = self:GetConditionString(condition),
        order = j,
        args = {
          -- triggerName = {
          --   name = L["Condition name"],
          --   type = 'input',
          --   -- width = 'full',
          --   get = function()
          --     return condition.name
          --   end,
          --   order = 1
          -- },
          triggerDelete = {
            name = L["Delete condition"],
            desc = L["Delete condition"],
            type = 'execute',
            func = function()
              self:DeleteCondition(condition, trigger)
              self:InitOptions()
            end,
            confirm = true,
            confirmText = L["Delete condition?"],
            order = 2
          },
          parametersHeader = {
            name = L["Parameters"],
            type = 'header',
            order = 3
          },  
          argumentNumber = {
            type = 'input',
            name = L["Argument number"],
            get = function()
              return condition.argumentNumber
            end,
            set = function(info, value)
              if not string.match(value, '^%d+$') then
                SharinganMainFrame:SetStatusText(L["Invalid argument number"])
                return
              end
              condition.argumentNumber = value
              self:InitOptions()
            end,
            order = 4
          },
          operator = {
            type = 'select',
            name = L["Operator"],
            values = {
              equals = L["Equals"],
              contains = L["Contains"],
              isMe = L["It's me!"],
            },
            sorting = {
              'equals',
              'contains',
              'isMe',
            },
            get = function()
              return condition.operator
            end,
            set = function(info, value)
              condition.operator = value
              self:InitOptions()
            end,
            order = 5
          },
          operand = {
            type = 'input',
            name = L["Operand"],
            desc = condition.operand,
            get = function()
              return condition.operand
            end,
            set = function(info, value)
              condition.operand = value
              self:InitOptions()
            end,
            order = 6
          }
        }
      }
    end
  end

  AceConfig:RegisterOptionsTable(self.name, options)
end

function Sharingan:OnInitialize()
  -- Called when the addon is loaded
  self.name = ADDON_NAME
  SharinganMainFrame = nil
  self.db = LibStub("AceDB-3.0"):New("SharinganDB", defaults, true)

  self:InitOptions()
  AceConfigDialog:AddToBlizOptions(self.name, self.name)

  self:RegisterChatCommand("sha", "ToggleWindow")
  self:RegisterChatCommand("sharingan", "ToggleWindow")

  self:RegisterEvent('COMBAT_LOG_EVENT_UNFILTERED')
end

function Sharingan:OnEnable()
  -- Called when the addon is enabled
  self:Print(L["Sharingan enabled!"])
end

function Sharingan:OnDisable()
  -- Called when the addon is disabled
end

function Sharingan:ToggleWindow(input)
  -- SharinganMainFrame:Show()
  -- SharinganMainFrame = self:BuildMainFrame("SharinganMainFrame")
  if not SharinganMainFrame or not SharinganMainFrame:IsVisible() then
    SharinganMainFrame = self:BuildMainFrame("SharinganMainFrame")
  else
    -- SharinganMainFrame:Release()
    -- print(SharinganMainFrame.status)
    SharinganMainFrame:Hide()
    -- print(SharinganMainFrame.status)
    -- AceGUI:Release(SharinganMainFrame)
    -- SharinganMainFrame:Fire('OnClose')
    SharinganMainFrame = nil
  end
end

function Sharingan:SetEnable(info, val)
  -- self.enabled = val
  self.db.global.enabled = val
end

function Sharingan:GetEnable(info)
  -- return self.enabled
  return self.db.global.enabled
end

function Sharingan:FindTrigger(name)
  local i, trigger
  for i, trigger in pairs(self.db.global.triggers) do
    if trigger.name == name then
      return i
    end
  end
end

function Sharingan:DeleteTrigger(triggerToRemove)
  -- local i, trigger
  -- for i, trigger in pairs(self.db.global.triggers) do
  --   if trigger.name == triggerToRemove.name then
  --     table.remove(self.db.global.triggers, i)
  --     return
  --   end
  -- end
  local i = self:FindTrigger(triggerToRemove.name)
  table.remove(self.db.global.triggers, i)
end

function Sharingan:TriggerNameExists(name)
  -- local i, trigger
  -- for i, trigger in pairs(self.db.global.triggers) do
  --   if trigger.name == name then
  --     return true
  --   end
  -- end
  -- return false
  local i = self:FindTrigger(name)
  if i then  
    return true
  else
    return false
  end
end

function Sharingan:FindCondition(name, trigger)
  local i, condition
  for i, condition in pairs(trigger.conditions) do
    if condition.name == name then
      return i
    end
  end
end

function Sharingan:ConditionNameExists(name, trigger)
  local i = self:FindCondition(name, trigger)
  if i then  
    return true
  else
    return false
  end
end

function Sharingan:DeleteCondition(condition, trigger)
  local i = self:FindCondition(condition.name, trigger)
  table.remove(trigger.conditions, i)
end

function table.map(f, t)
  local index
  -- for index, value in ipairs(t) do -- ipairs() doesn't go over the nil values...
  for index = 1, #t do
    value = t[index]
    -- print(index, value)
    if value == nil then
      t[index] = 'nil'
    else
      t[index] = f(value)
    end
  end
  -- return t
end

function Sharingan:COMBAT_LOG_EVENT_UNFILTERED()
  -- process the event
  if not self.db.global.enabled then
    return
  end

  if self.db.global.recordingEnabled then
    local info = {CombatLogGetCurrentEventInfo()}
    table.map(function(e) return tostring(e) end, info)
    local line = '- ' .. table.concat(info, ', ')
    -- print(line)
    self.db.global.events = self.db.global.events .. line .. '\n'
  end

  if self.db.global.notificationEnabled then
    local info = {CombatLogGetCurrentEventInfo()}
    local i, trigger
    for i, trigger in pairs(self.db.global.triggers) do      
      if self:InfoMatchTrigger(info, trigger) then
        self:TriggerAction(info, trigger)
      end
    end
  end
end

function Sharingan:InfoMatchTrigger(info, trigger)
  local i, condition
  for i, condition in pairs(trigger.conditions) do
    if not self:InfoMatchCondition(info, condition) then
      return false
    end
  end
  return true
end

function Sharingan:InfoMatchCondition(info, condition)
  local argumentNumber = tonumber(condition.argumentNumber)
  local argumentValue = info[argumentNumber] or ""
  -- print(argumentValue)
  local operator = condition.operator or ""
  local operand = condition.operand or ""
  if operator == 'equals' then
    if argumentValue == operand then
      return true
    end
  elseif operator == 'contains' then
    if string.find(argumentValue, operand) then
      return true
    end
  elseif operator == 'isMe' then
    local playerName = UnitName('player');
    if argumentValue == playerName then
      return true
    end
  end
  return false
end

function Sharingan:TriggerAction(info, trigger)
  local message = trigger.action.message or ""

  if not message then
    return
  end

  local i, v
  for i, v in pairs(info) do
    -- print(i, v)
    message = string.gsub(message, '{'..i..'}', tostring(v))
  end
  -- print(message)

  self:SendMessage(message, trigger.action.channel)
end

function Sharingan:SendMessage(message, channel)
  -- print(message, channel)
  if not channel or channel == 'none' then
    return
  end

  if channel == 'default' then
    channel = self:GetDefaultChannel()
  end
  -- print(channel)

  if channel == 'chatWindow' then
    self:Print(message)
  elseif channel == 'sayChannel' then
    -- print(message, 'SAY')
    -- Patch 8.2.5 (2019-09-24): Partially protected; "SAY" and "YELL" must be triggered by hardware event
    -- SAY/YELL seems hardware event protected while outdoors but not inside instances/raids
    SendChatMessage(message, 'SAY')
  elseif channel == 'groupChannel' then
    SendChatMessage(message, 'PARTY')
  elseif channel == 'raidChannel' then
    SendChatMessage(message, 'RAID')
  elseif channel == 'warningChannel' then
    SendChatMessage(message, 'RAID_WARNING')
  elseif channel == 'battlegroundChannel' then
    -- SendChatMessage(message, 'BATTLEGROUND')
    SendChatMessage(message, 'INSTANCE_CHAT')
  end
end

function Sharingan:GetDefaultChannel()
  local inInstance, instanceType = IsInInstance()

  if inInstance then
    if instanceType == 'pvp' then
      return self.db.global.defaultBattlegroundChannel
    elseif IsInRaid() then
      return self.db.global.defaultRaidInstanceChannel
    elseif IsInGroup() then    
      return self.db.global.defaultGroupInstanceChannel
    else
      return self.db.global.defaultSoloInstanceChannel
    end
  else
    if IsInRaid() then
      return self.db.global.defaultRaidChannel
    elseif IsInGroup() then    
      return self.db.global.defaultGroupChannel
    else
      return self.db.global.defaultSoloChannel
    end
  end
end

function Sharingan:GetConditionString(condition)
  local argumentNumber = condition.argumentNumber or ""
  local operator = condition.operator or ""
  local operand = condition.operand or ""

  if operator == 'equals' then
    return string.format(L["{%s} = %s"], argumentNumber, operand)
  elseif operator == 'contains' then
    return string.format(L["{%s} contains %s"], argumentNumber, operand)
  elseif operator == 'isMe' then
    return string.format(L["{%s} is me!"], argumentNumber, operand)
  end
end
